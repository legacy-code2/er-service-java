**Pass Null**

Потребность: добавить condition для /inboundPatients

1. Найти точку изменений 
2. Начать писать unit-тест
3. Предать null в тесте в качестве зависимости
4. Извлечь метод для SUT
5. Зафиксировать поведение в тесте
6. Написать красный тест на желаемое поведение
7. Написать необходимый код

```xml
<Inbound>
	<Patient>
		<TransportId>1</TransportId>
		<Name>John Doe</Name>
		<Condition>heart arrhythmia</Condition>
		<Priority>YELLOW</Priority>
		<Birthdate></Birthdate>
	</Patient>
</Inbound>
```

**Subclass and override**

Потребность: добавить отправку сообщений для пациентов с priority=Yellow и condition="heart arrhythmia"

1. Extract Interface InboundPatientSource
1. Написать unit-тест для AlertScanner.scan
```java
    private Patient createPatient(int transportId, Priority priority, String condition) {
        Patient patient = new Patient();
        patient.setTransportId(transportId);
        patient.setPriority(priority);
        patient.setCondition(condition);
        return patient;
    }
```
3. Создать тестовый дублёр для InboundPatientSource
```java
private class InboundPatientDouble implements InboundPatientSource{

        ArrayList<Patient> patients = new ArrayList<>();

        @Override
        public List<Patient> currentInboundPatients() {
            return patients;
        }

        @Override
        public void informOfPatientArrival(int transportId) {

        }

        public void simulateNewPatient(Patient patient) {
            patients.add(patient);
        }
    }
```
4. Создать подкласс для AlertScanner 
``` 
// сделать alertForNewCriticalPatient protected)
// Переопределить alertForNewCriticalPatient 
// public patients
// patients.add(patient);
```
5. Написать красный тест на новое поведение
6. Написать код который требует этот тест